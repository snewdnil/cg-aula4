#version 130

uniform float uVertexScaleX;
uniform float uVertexScaleY;

in vec2 aPosition;
in vec3 aColor;

out vec3 vColor;

void main() {
  gl_Position = vec4(aPosition.x * uVertexScaleX, aPosition.y * uVertexScaleY, 0,1);
  vColor = aColor;
}
