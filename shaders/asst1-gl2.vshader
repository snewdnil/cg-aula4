uniform float uVertexScaleX;
uniform float uVertexScaleY;

attribute vec2 aPosition;
attribute vec3 aColor;

varying vec3 vColor;

void main() {
  gl_Position = vec4(aPosition.x * uVertexScaleX, aPosition.y * uVertexScaleY, 0,1);
  vColor = aColor;
}
