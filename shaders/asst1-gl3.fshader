#version 130

uniform float uVertexScaleX;
uniform float uVertexScaleY;

in vec3 vColor;

out vec4 fragColor;

void main(void) {
  vec4 color = vec4(vColor.x, vColor.y, vColor.z, 1);
  fragColor = color;
}
