BASE = asst1

all: $(BASE)

#LDFLAGS += -L/usr/X11R6/lib
LIBS += -lGL -lGLU -lglut
CXXFLAGS += -g

CXX = g++ 

OBJ = $(BASE).o glsupport.o

$(BASE): $(OBJ)
	$(LINK.cpp) -o $@ $^ $(LIBS) -lGLEW 

clean:
	rm -f $(OBJ) $(BASE)
