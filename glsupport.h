#ifndef GLSUPPORT_H
#define GLSUPPORT_H

#include <iostream>
#include <stdexcept>

#include <GL/glew.h>
# include <GL/glut.h>


// Check if there has been an error inside OpenGL and if yes, print the error and
// through a runtime_error exception.
void checkGlErrors();

// Reads and compiles a pair of vertex shader and fragment shader files into a
// GL shader program. Throws runtime_error on error
void readAndCompileShader(GLuint programHandle,
                          const char *vertexShaderFileName, const char *fragmentShaderFileName);

// Link two compiled vertex shader and fragment shader into a GL shader program
void linkShader(GLuint programHandle, GLuint vertexShaderHandle, GLuint fragmentShaderHandle);

// Reads and compiles a single shader (vertex, fragment, etc) file into a GL
// shader. Throws runtime_error on error
void readAndCompileSingleShader(GLuint shaderHandle, const char* shaderFileName);

// Classes inheriting Noncopyable will not have default compiler generated copy
// constructor and assignment operator
class Noncopyable {
protected:
  Noncopyable() {}
  ~Noncopyable() {}
private:
  Noncopyable(const Noncopyable&);
  const Noncopyable& operator= (const Noncopyable&);
};

// Light wrapper around a GL shader (can be geometry/vertex/fragment shader)
// handle. Automatically allocates and deallocates. Can be casted to GLuint.
class GlShader : Noncopyable {
protected:
  GLuint handle_;

public:
  GlShader(GLenum shaderType) {
    handle_ = glCreateShader(shaderType); // create shader handle
    if (handle_ == 0)
      throw std::runtime_error("glCreateShader fails");
    checkGlErrors();
  }

  ~GlShader() {
    glDeleteShader(handle_);
  }

  // Casts to GLuint so can be used directly by glCompile etc
  operator GLuint() const {
    return handle_;
  }
};

// Light wrapper around GLSL program handle that automatically allocates
// and deallocates. Can be casted to a GLuint.
class GlProgram : Noncopyable {
protected:
  GLuint handle_;

public:
  GlProgram() {
    handle_ = glCreateProgram();
    if (handle_ == 0)
      throw std::runtime_error("glCreateProgram fails");
    checkGlErrors();
  }

  ~GlProgram() {
    glDeleteProgram(handle_);
  }

  // Casts to GLuint so can be used directly by glUseProgram and so on
  operator GLuint() const {
    return handle_;
  }
};


// Light wrapper around a GL texture object handle that automatically allocates
// and deallocates. Can be casted to a GLuint.
class GlTexture : Noncopyable {
protected:
  GLuint handle_;

public:
  GlTexture() {
    glGenTextures(1, &handle_);
    checkGlErrors();
  }

  ~GlTexture() {
    glDeleteTextures(1, &handle_);
  }

  // Casts to GLuint so can be used directly by glBindTexture and so on
  operator GLuint () const {
    return handle_;
  }
};

// Light wrapper around a GL buffer object handle that automatically allocates
// and deallocates. Can be casted to a GLuint.
class GlBufferObject : Noncopyable {
protected:
  GLuint handle_;

public:
  GlBufferObject() {
    glGenBuffers(1, &handle_);
    checkGlErrors();
  }

  ~GlBufferObject() {
    glDeleteBuffers(1, &handle_);
  }

  // Casts to GLuint so can be used directly glBindBuffer and so on
  operator GLuint() const {
    return handle_;
  }
};


#endif
